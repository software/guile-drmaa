;;; Copyright © 2021 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;; Commentary:
;;;
;;; This script extracts docstrings from a module and spits out a
;;; TexInfo file.
;;;
;;;; Code:

(use-modules (ice-9 match)
             (ice-9 session))

(define (arguments->string proc)
  (match (procedure-arguments proc)
    ((('required . req-names)
      ('optional . opt-names)
      ('keyword . kw-names)
      ('allow-other-keys? . any)
      ('rest . rest))
     (string-join
      (list
       (format #false "~{@var{~a}~^ ~}" req-names)
       (format #false "~:[[~{@var{~a}~^ ~}]~;~]" (null? opt-names) opt-names)
       (format #false "~:[[~{@var{~a}~^ ~}]~;~]" (null? kw-names) (map car kw-names))
       (format #false "~@[@var{~a} ...~]" rest))))))

(define (fancy-docstring proc known-procedures)
  (define arguments
    (match (procedure-arguments proc)
      ((('required . req-names)
        ('optional . opt-names)
        ('keyword . kw-names)
        ('allow-other-keys? . any)
        ('rest . rest))
       (let ((kws (map (compose keyword->symbol car)
                       kw-names)))
         (map symbol->string
              (append req-names opt-names kws
                      (if rest (list rest) '())))))))
  (define (argument? word)
    (member (string-downcase word) arguments))
  (define* (maybe-wrap word #:optional (trail ""))
    (let ((word* (string-drop-right word (string-length trail))))
      (if (or (member word* '("#TRUE" "#FALSE" "*"))
              (member word* known-procedures)
              (argument? word*))
          (string-append "@var{" (string-downcase word*) "}" trail)
          (string-append word* trail))))
  (let ((words (string-tokenize (procedure-documentation proc))))
    (string-join (map (lambda (word)
                        (cond
                         ((string-suffix? "." word)
                          (maybe-wrap word "."))
                         ((string-suffix? "," word)
                          (maybe-wrap word ","))
                         ((string-suffix? ";" word)
                          (maybe-wrap word ";"))
                         (else
                          (maybe-wrap word))))
                      words))))

(define (docs module)
  (define known-procedures
    (module-map (lambda (sym var)
                  (string-upcase (symbol->string sym)))
                (resolve-interface module)))
  (let ((unsorted
         (filter identity
                 (module-map
                  (lambda (sym var)
                    (false-if-exception
                     (let ((proc (variable-ref var)))
                       (cons sym (format #false
                                         "\
@cindex ~a
@deffn {Scheme Procedure} ~a ~a
~a
@end deffn
" sym sym
(arguments->string proc)
(fancy-docstring proc known-procedures))))))
                  (resolve-interface module)))))
    (sort unsorted (lambda (a b)
                     (string< (symbol->string (car a))
                              (symbol->string (car b)))))))

(define (main . args)
  (match (command-line)
    ((_ module file)
     (let ((directory (dirname file)))
       (unless (file-exists? directory)
         (mkdir directory)))
     (with-output-to-file file
       (lambda ()
         (for-each (match-lambda
                     ((sym . doc)
                      (display doc)
                      (newline)))
                   (docs (call-with-input-string module read))))))))
