;;; Guile DRMAA --- Guile bindings for DRMAA
;;; Copyright © 2021 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This file is part of Guile DRMAA.
;;;
;;; Guile DRMAA is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile DRMAA is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile DRMAA.  If not, see <http://www.gnu.org/licenses/>.

(define-module (drmaa errors)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:export (&drmaa-error
            drmaa-error?
            drmaa-error-code

            &formatted-message
            formatted-message?
            formatted-message-string
            formatted-message-arguments))

(define-condition-type &drmaa-error &error
  drmaa-error?
  (code drmaa-error-code))


(define-condition-type &formatted-message &error
  formatted-message?
  (format    formatted-message-string)
  (arguments formatted-message-arguments))
