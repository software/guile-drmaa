(use-modules
 (guix packages)
 ((guix licenses) #:prefix license:)
 (guix download)
 (guix build-system gnu)
 (gnu packages)
 (gnu packages autotools)
 (gnu packages base)
 (gnu packages mes)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages pkg-config)
 (gnu packages texinfo))

(package
  (name "guile-drmaa")
  (version "0.1")
  (source "./guile-drmaa-0.1.tar.gz")
  (build-system gnu-build-system)
  (native-inputs
   (list autoconf
         automake
         pkg-config
         texinfo
         sed))
  (inputs
   (list guile-3.0))
  (propagated-inputs
   (list guile-bytestructures nyacc))
  (synopsis "")
  (description "")
  (home-page "")
  (license license:gpl3+))

